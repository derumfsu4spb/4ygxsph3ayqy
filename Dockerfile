# Use an official base image, like Ubuntu
FROM ubuntu:latest

# Install stress-ng
RUN apt-get update && \
    apt-get install -y stress-ng

# Command to run the stress test without a timeout and with max CPUs
CMD ["stress-ng", "--cpu", "0", "--metrics-brief"]
